import {HttpException, HttpStatus, Injectable, NestMiddleware} from '@nestjs/common';
import {JwtService} from '@nestjs/jwt';

@Injectable()
export class AuthMiddleware implements NestMiddleware {

    constructor(private jwtService: JwtService) {
    }

    use(req: any, res: any, next: () => void) {
        try {
            const token = req.headers?.authorization?.split(' ')[1];
            if (!token) throw new HttpException('No token present', HttpStatus.FORBIDDEN)
            const publicKey = process.env.PUBLIC_KEY;
            const decodedToken: any = this.jwtService.verify(token, {publicKey});
            const userId = decodedToken?.id;
            if (!decodedToken || typeof userId !== "number")
                throw new HttpException('Invalid token', HttpStatus.FORBIDDEN)
            res.locals.userId = userId;
            next();
        } catch (e: any) {
            throw new HttpException(e.message, HttpStatus.FORBIDDEN)
        }
    }
}
